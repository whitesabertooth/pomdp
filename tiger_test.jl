Pkg.add("POMDPs")
Pkg.add("POMDPModels")
#import POMDPs
#POMDPs.add("ARDESPOT")

# using POMDPs, POMDPModels, POMDPToolbox, ARDESPOT
#
# pomdp = TigerPOMDP()
#
# solver = DESPOTSolver(bounds=(-20.0, 0.0))
# planner = solve(solver, pomdp)
#
# for (s,a,o) in stepthrough(pomdp, planner, "sao", max_steps=10)
#     println("State was $s,")
#     println("action $a was taken,")
#     println("and observation $o was received.\n")
# end
